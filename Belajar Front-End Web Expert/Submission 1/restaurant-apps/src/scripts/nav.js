/* eslint-disable no-undef */
const hamburgerButtonElement = document.querySelector('#hamburger');
const drawerElement = document.querySelector('#drawer');
const hero = document.querySelector('.hero');
const main = document.querySelector('main');

hamburgerButtonElement.addEventListener('click', (event) => {
  drawerElement.classList.toggle('open');
  event.stopPropagation();
});

hero.addEventListener('click', () => {
  drawerElement.classList.remove('open');
});

main.addEventListener('click', () => {
  drawerElement.classList.remove('open');
});
