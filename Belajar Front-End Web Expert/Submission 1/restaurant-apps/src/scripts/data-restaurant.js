/* eslint-disable no-undef */
import data from '../DATA.json';

let explore = '';

data.restaurants.forEach((restaurants) => {
  explore += `<div class="box-explore">
                <div tabindex="0" class="image">
                    <div class="badge">Kota ${restaurants.city}</div>
                    <div class="products-thumbnail">
                        <img src="${restaurants.pictureId}" alt="${restaurants.name} Gambar" />
                    </div>
                </div>
                <p tabindex="0" class="rating">Rating: ${restaurants.rating}</p>
                <h3 tabindex="0" class="title">${restaurants.name}</h3>
                <p tabindex="0" class="subtitle">${restaurants.description}</p>
              </div>`;
});

document.getElementById('explore-content').innerHTML = explore;
