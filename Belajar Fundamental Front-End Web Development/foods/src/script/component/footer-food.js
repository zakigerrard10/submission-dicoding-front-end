class FooterFood extends HTMLElement {
  constructor() {
    super();
    this.shadowDOM = this.attachShadow({ mode: "open" });
  }

  connectedCallback() {
    this.render();
  }

  render() {
    const footerElement = document.createElement("footer");
    this.shadowDOM.innerHTML = `
        <style>
            *{
                font-family: "Poppins", sans-serif;
            }
            footer {
                margin-top: 100px;
                background: #fafafa;
                text-align: center;
                align-items: center;
                justify-content: center;
                font-size: 32px;
                padding: 45px 80px 15px 80px;
                font-weight: 700;
                color: #30475e;
            }
            footer span {
                color: #0047b1;
            }
            @media (max-width: 1300px) {
                footer{
                    font-size : 24px
                }
            }
            @media (max-width: 630px) {
                footer{
                    font-size : 18px
                }
            }
        </style>
        <footer>
            <p>Belajar Pemrograman Web Fundamental &#169; 2021, <span>Ahmad Zaki Yamani</span></p>
        </footer>    
        `;
    footerElement.appendChild(footerElement);
  }
}

customElements.define("footer-food", FooterFood);
